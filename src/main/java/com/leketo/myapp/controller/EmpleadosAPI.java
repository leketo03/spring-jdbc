package com.leketo.myapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.leketo.myapp.bean.Empleado;
import com.leketo.myapp.service.EmpleadoService;

import io.swagger.annotations.ApiOperation;

@RestController
public class EmpleadosAPI {

	@Autowired
	private EmpleadoService service;
	
	@ApiOperation(value="Consulta de empleados",notes="Realiza la consulta de de todos los empleados" ,nickname="obtenerEmpleados")
	@RequestMapping(value="/empleados", method=RequestMethod.GET)
	public List<Empleado> getAllEmpleados(){
		return service.getAll();
	}
	
	@RequestMapping(value="/empleados/{id}", method=RequestMethod.GET)
	public Empleado getAllEmpleados(
			@PathVariable("id") Long id
			){
		return service.getById(id);
	}
}
