package com.leketo.myapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leketo.myapp.bean.Empleado;
import com.leketo.myapp.dao.EmpleadoDAO;

@Service
public class EmpleadoService {
	
	@Autowired
	private EmpleadoDAO dao;
	
	public List<Empleado> getAll(){
		return dao.getAll();
	}
	
	public Empleado getById(Long id){
		return dao.getById(id);
	}

}
