package com.leketo.myapp.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.leketo.myapp.bean.Empleado;

@Repository
public class EmpleadoDAO extends JdbcDaoSupport{
	
	@Autowired
	public EmpleadoDAO(DataSource ds) {
		setDataSource(ds);
	}
	
  private final static String GET_ALL = "select * from empleados";
  private final static String GET_BY_ID = "select * from empleados where id = ?";
  


  public List<Empleado> getAll(){
	  return getJdbcTemplate().query(GET_ALL, new EmpleadosMapper());
  }

  public Empleado getById(Long id){
	  return getJdbcTemplate().queryForObject(GET_BY_ID, new EmpleadosMapper(), id);
  }
  
  
  
  private class EmpleadosMapper implements RowMapper<Empleado>{

	@Override
	public Empleado mapRow(ResultSet rs, int row) throws SQLException {
		Empleado e = new Empleado();
		e.setId(rs.getLong("id"));
		e.setNombre(rs.getString("nombre"));
		e.setFechaIngreso(rs.getDate("fecha_ingreso"));
		e.setSalario(rs.getBigDecimal("salario"));
		return e;
	} 
  }
}
