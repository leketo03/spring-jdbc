package com.leketo.myapp.codegen;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.leketo.myapp.bean.ErrorBody;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
public class CodegenConfiguration {
 
	/* TypeResolver
	 * El paquete admite la lectura y escritura de Java Beans est�ndar: 
	 * es decir, un subconjunto de POJO que define setters / getters (comenzando con Jackson-jr 2.8) puede usar publiccampos alternativamente ).
	 * 
	 * https://github.com/FasterXML/jackson-jr
	 */
	@Autowired
	private TypeResolver typeResolver;
	
	/*@Bean 
	 * Indica que un m�todo produce un bean que ser� administrado por el contenedor Spring.
	 */
	@Bean
	public Docket api() {
		
		List<ResponseMessage> errors = new ArrayList<ResponseMessage>();
		//400
		errors.add(new ResponseMessageBuilder()
				.code(HttpStatus.BAD_REQUEST.value())
				.message("Invalid Request")
				.responseModel(new ModelRef("ErrorBody"))
				.build());
		//401
		errors.add(new ResponseMessageBuilder()
				.code(HttpStatus.UNAUTHORIZED.value())
				.message("Security exception")
				.responseModel(new ModelRef("ErrorBody"))
				.build());
		//500
		errors.add(new ResponseMessageBuilder()
				.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.message("Unexpected server exception")
				.responseModel(new ModelRef("ErrorBody"))
				.build());
		
		
		/*Docket
		�*Un generador que est� destinado a ser la interfaz principal en el marco swagger springmvc.
		�*Proporciona m�todos predeterminados y convenientes para la configuraci�n.
		�*/
		return new Docket(DocumentationType.SWAGGER_2)
				.select() // Inicia un constructor para la selecci�n de API.
				.apis(RequestHandlerSelectors.any()) //Cualquier RequestHandler satisface esta condici�n
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters( //Agrega par�metros predeterminados que se aplicar�n a todas las operaciones.
                		Lists.newArrayList(new ParameterBuilder()
                				.name("X-RshkMichi-ApiKey")
                				.description("API KEY de la aplicaci�n solicitante.")
                				.modelRef(new ModelRef("string"))
                				.parameterType("header")
                				.required(true)
                	            .build()
                	            ))
                .produces(Sets.newHashSet("application/json", "application/xml"))
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, errors) //Reemplaza los mensajes de respuesta http predeterminados en el nivel de m�todo de solicitud http.
                .globalResponseMessage(RequestMethod.POST, errors)
                .globalResponseMessage(RequestMethod.PUT, errors)
                .globalResponseMessage(RequestMethod.DELETE, errors)
                .additionalModels(typeResolver.resolve(ErrorBody.class)) //M�todo para agregar modelos adicionales
      	        .apiInfo(apiInfo());
	}
	
	private ApiInfo apiInfo() {
	    ApiInfo apiInfo = new ApiInfo(
	      "Spring Framework",
	      "API que provee la la logica para el manejo de la aplicacion",
	      "v"+getClass().getPackage().getImplementationVersion(),
	      "",
	      new Contact("Leketo", "www.leketo.com", "leketo03@gmail.com"),
	      "",
	      "");
	    return apiInfo;
	}
	
}
