package com.leketo.myapp.exception;

//Tipo de errores posibles
public enum APIExceptionType {
	INTERNAL,
	SECURITY,
	COMMUNICATION,
	DATABASE,
	APPLICATION
}