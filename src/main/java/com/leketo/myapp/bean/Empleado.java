package com.leketo.myapp.bean;

import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description="Contiene los datos de un empleado")
public class Empleado {

	@ApiModelProperty(value="Id")
	private Long id;
	
	@ApiModelProperty(value="Nombre completo")
	private String nombre;
	
	@ApiModelProperty(value="Fecha de ingreso")
	private Date fechaIngreso;
	
	@ApiModelProperty(value="Salario")
	private BigDecimal salario;
	
	public Empleado() {}
	public Empleado(Long id, String nombre, Date fechaIngreso, BigDecimal salario) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.fechaIngreso = fechaIngreso;
		this.salario = salario;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public BigDecimal getSalario() {
		return salario;
	}
	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
	@Override
	public String toString() {
		return "Empleado [id=" + id + ", nombre=" + nombre + ", salario=" + salario + "]";
	}
	
}
